# Useful Stuff Ep1: Web Scraper

Prerequisites:

```sh
$ go get -u -v github.com/PuerkitoBio/goquery
$ go get -u -v github.com/hpcloud/tail
```

Until `hpcloud/tail` not implementing the `tail -n` equivalent, you need GNU `tail`.

Usage:

```sh
$ go build -o web-scraper.exe ./src
$ web-scraper.exe
```

Docker containers:

```sh
$ docker network create tor-proxy
$ docker container prune -f && docker image prune -f && docker rmi docker_app
```

TOR:

https://github.com/rwestergren/docker-compose-tor-demo

Respect the robots.txt: http://www.imdb.com/robots.txt

URL structure:

```sh
http://ceginformacio.creditreform.hu/cr9310021446
http://ceginformacio.creditreform.hu/cr9310000000
http://ceginformacio.creditreform.hu/cr9310000001
http://ceginformacio.creditreform.hu/cr9310000002
http://ceginformacio.creditreform.hu/cr9319999999
```

##### Notes

```go
/*
http://www.ryzhak.com/creating-html-parser-using-golang/

Proxy support:
https://github.com/tebeka/selenium
*/

/*
TODO:
- Implement TOR or ONION routing:
	http://www.devdungeon.com/content/making-tor-http-requests-go
	https://gist.github.com/Yawning/bac58e08a05fc378a8cc

	https://stackoverflow.com/questions/40328025/tcp-connection-over-tor-in-golang

	https://play.golang.org/p/l0iLtkD1DV
*/

// COMMAND THAT STARTED TOR: $ tor -f /etc/tor.conf
// $ tail -n 100 /tmp/var/log/messages
// $ tail -n 1000 /tmp/var/log/messages | grep Tor
// $ cat /etc/tor.conf
// $ cat /tmp/etc/tor.conf
// $ cat /usr/etc/tor/torrc
// $ service tor status
//
// $ curl -v --socks5 127.0.0.1:9050 https://check.torproject.org/
// $ curl --insecure -v --socks5-hostname 127.0.0.1:9050 https://check.torproject.org/
// $ curl -x socks5://127.0.0.1:9050 https://check.torproject.org/
// $ curl --socks5 127.0.0.1:9050 http://stackoverflow.com/
//
// $ SOCKS_SERVER=127.0.0.1:9050 wget -O- https://check.torproject.org/
// $ set SOCKS_SERVER=127.0.0.1:9050 && wget --no-check-certificate -O- https://check.torproject.org/
// $ telnet 192.168.2.1 23
// USER: root
// PASS: <The one that you use for login in the GUI>
```
