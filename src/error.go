package main

import "log"

// Check : error handling
func Check(e error) {
	if e != nil {
		//panic(e)
		log.Fatal(e)
	}
}
