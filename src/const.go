package main

// Global variables
const (
	TOR        = true // default: true
	ProxyAddr  = "127.0.0.1:9050"
	URLStart   = "http://ceginformacio.creditreform.hu/cr931"
	ElemParent = "body .main .row .bordered_data tbody tr"
	ElemChild  = "td:last-child"
	CrawlDelay = 0 // default: 3
)
