package main

import (
	"bytes"
	"encoding/csv"
	"log"
	"os/exec"
	"strconv"
)

// Tail : Returns the last line of the file
func Tail(filename string) int {
	out, err := exec.Command("tail", "-n", "1", filename).Output()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Printf("%s\n", out)

	// CSV parsing
	r := csv.NewReader(bytes.NewReader(out))
	r.Comma = '\t'
	//r.Comment = '#'
	r.LazyQuotes = true
	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Printf("%s\n", records[0][2][3:])

	k, _ := strconv.Atoi(records[0][2][3:])
	//fmt.Printf("%d\n", k)

	return k
}
